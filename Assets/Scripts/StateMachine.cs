﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine : MonoBehaviour
{


      public enum States{
          BASIC,
          USINGCOMPUTER
      }

      public States currentState;

      public GameObject objectInUse;

      // Use this for initialization
      void Start () {
          currentState = States.BASIC;
      }

      // Update is called once per frame
      void Update () {
          switch(currentState){
          case(States.BASIC):
              //Debug.Log ("Basic state");
              break;
          case(States.USINGCOMPUTER):
              //currentState = States.USINGCOMPUTER;
              //Debug.Log ("Using "+objectInUse);
              break;
          }
      }

}
