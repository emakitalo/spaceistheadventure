﻿//  GeneralObjectRotator


using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class GeneralObjectRotator : MonoBehaviour {

	public float startDelay;

	public float rotatingSpeedX;
	public float rotatingSpeedY;
	public float rotatingSpeedZ;

	public bool runInEditor;

/*
	void Start() {
		if (rotatingSpeedX==0f && rotatingSpeedY==0f && rotatingSpeedZ==0f) {
			LDebug.Log("Destroying general object rotator script (all rotate speeds are 0)");
			Destroy(this);
		}
	}
*/

#if UNITY_EDITOR

	void OnEnable() {
		if (!Application.isPlaying) {
			UnityEditor.EditorApplication.update += EditorUpdate;
		}
	}

	void OnDisable() {
		if (!Application.isPlaying) {
			UnityEditor.EditorApplication.update -= EditorUpdate;
		}
	}

	void EditorUpdate() {
		if (runInEditor) {
			doUpdate();
		}
	}

#endif

	void Update() {
		if (Application.isPlaying) {
			doUpdate();
		}
	}

	private void doUpdate() {
		if (startDelay>0f) {
			startDelay-=Time.deltaTime;
			if (startDelay<0f) {
				startDelay=0f;
			}
		}
		if (startDelay<=0f) {
			if (rotatingSpeedX!=0f) {
				rotateStep(Vector3.right,rotatingSpeedX,Time.deltaTime);
			}
			if (rotatingSpeedY!=0f) {
				rotateStep(Vector3.up,rotatingSpeedY,Time.deltaTime);
			}
			if (rotatingSpeedZ!=0f) {
				rotateStep(Vector3.forward,rotatingSpeedZ,Time.deltaTime);
			}
		}
	}

	private void rotateStep(Vector3 axis, float rotatingSpeed, float deltaTime) {
		this.transform.RotateAround(this.transform.position,axis,rotatingSpeed*deltaTime);
	}

}
