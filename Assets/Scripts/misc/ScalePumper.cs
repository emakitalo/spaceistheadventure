﻿//    ScalePumper


using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScalePumper : MonoBehaviour {

	public bool autoStart=false;

	private RectTransform rectTransform;

	public float normalScale=1f;
	public float maxScale=1.1f;
	public float speed=1f;

	private bool pumping;
	private float counter;

	void Awake() {
		if (rectTransform==null) {
			rectTransform=this.GetComponent<RectTransform>();
//			LDebug.Log("ScalePumper.Awake(): RectTransform is not defined, trying to get default: "+rectTransform);
		}
		if (autoStart) {
			startPumping();
		}
	}

	void Update() {
		if (pumping) {
			counter += Time.deltaTime * speed;
			float p = Mathf.Abs(Mathf.Sin(counter));
			if (rectTransform != null) {
				rectTransform.localScale = Vector3.one * (normalScale * (1f - p) + maxScale * p);
			}
		}
	}

	public void startPumping() {
		if (!pumping) {
			counter=0f;
			pumping=true;
		}
	}

	public void stopPumping() {
		pumping=false;
		rectTransform.localScale=Vector3.one*normalScale;
	}

}
