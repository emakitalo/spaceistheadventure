﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Raycasting : MonoBehaviour
{

    public float range;
    RaycastHit hit;

    public GameObject hand;
    public GameObject dot;

    private StateMachine stateMachineScript;

    private string touchableString;

    //public touchableScript = GetComponent<Wander>();
    //touchableScript.enabled = true;

    // Start is called before the first frame update
    void Start()
    {
      dot.SetActive(true);
      hand.SetActive(false);

      stateMachineScript = GetComponent<StateMachine>();
    }

    // Update is called once per frame
    void Update()
    {
        Debug.DrawRay(this.transform.position, this.transform.forward * range, Color.green);

        if(Physics.Raycast(this.transform.position, this.transform.forward, out hit, range))
            {
                //Debug.Log ("Hit: "+hit.collider.gameObject.name);
                if (hit.collider.gameObject.GetComponent<Touchable>() == true)
                    {
                        //Debug.Log("Touchable"+hit.collider.gameObject.GetComponent<Touchable>().whichTouchable);
                        dot.SetActive(false);
                        hand.SetActive(true);

                        if(Input.GetMouseButtonDown(0))
                          {
                            stateMachineScript.objectInUse=hit.collider.gameObject;
                            stateMachineScript.currentState=StateMachine.States.USINGCOMPUTER;
                            touchableString = hit.collider.gameObject.GetComponent<Touchable>().whichTouchable.ToString();

                            Debug.Log("");
                          }
                    }
            }
        else
            {
              dot.SetActive(true);
              hand.SetActive(false);
            }

    }
}
